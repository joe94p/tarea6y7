package com.faccigilberthplazamendoza.sensores4d;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActividadSensorAcelerometro extends AppCompatActivity implements SensorEventListener {

    TextView lblTexto;
    SensorManager sensorManager;
    private Sensor acelerometro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_acelerometro);

        lblTexto = (TextView)findViewById(R.id.lblTexto);

        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        acelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float x,y,z;

        x = event.values[0];
        y = event.values[1];
        z = event.values[2];

        lblTexto.setText("");
        lblTexto.append("\n El Valor de X: " +x+ "\n El Valor de Y:"+y+ "\n El Valor Z:" +z);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,acelerometro,sensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

}

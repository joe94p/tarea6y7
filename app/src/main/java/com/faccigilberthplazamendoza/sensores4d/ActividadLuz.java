package com.faccigilberthplazamendoza.sensores4d;

import android.graphics.Camera;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadLuz extends AppCompatActivity {

    Button luz;
    android.hardware.Camera camera;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_luz);

        luz = (Button)findViewById(R.id.btnLuz);

        luz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera = android.hardware.Camera.open();
                android.hardware.Camera.Parameters parameters = camera.getParameters();
                parameters.setFlashMode(android.hardware.Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(parameters);
                camera.startPreview();
            }
        });

    }
}
